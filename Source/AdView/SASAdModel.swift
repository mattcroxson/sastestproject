//
//  SASAdModel.swift
//  SASTestProject
//
//  Created by Matt Croxson on 26/8/19.
//  Copyright © 2019 carsales.com Ltd. All rights reserved.
//

import Foundation

struct SASAdModel: Codable {
    let backgroundColour: String?
    let baseDomain: String?
    let customerId: String?
    let width: Int?
    let height: Int?
    let tags: [KeyValuePairModel]?
}

struct KeyValuePairModel: Codable {
    let key: String?
    let value: String?
}
