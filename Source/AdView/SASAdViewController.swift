//
//  SASAdViewController.swift
//  SASTestProject
//
//  Created by Matt Croxson on 26/8/19.
//  Copyright © 2019 carsales.com Ltd. All rights reserved.
//

import SASIA_SDK
import UIKit

class SASAdViewController: UIViewController {

    // MARK: - Outlets

    @IBOutlet private var adContainer: UIView!
    @IBOutlet private var adContainerHeightConstraint: NSLayoutConstraint!
    @IBOutlet private var adContainerWidthConstraint: NSLayoutConstraint!

    // MARK: - Properties

    private let jsonDecoder = JSONDecoder()
    private var viewModel: SASAdViewModel?
    private var adViewSize: CGSize {
        guard let width = viewModel?.adWidth,
            let height = viewModel?.adHeight else {
                return .zero
        }

        return CGSize(width: CGFloat(width), height: CGFloat(height))
    }

    private lazy var sasAdModel: SASAdModel? = {
        let fileName = "SASAdModel"
        let fileExtension = "json"

        guard let fileUrl = Bundle.main.url(forResource: fileName,
                                            withExtension: fileExtension) else { return nil }
        do {
            let fileData = try Data(contentsOf: fileUrl)
            let fileModel = try jsonDecoder.decode(SASAdModel.self, from: fileData)
            return fileModel
        } catch {
            simpleAlert(error.localizedDescription)
            return nil
        }
    } ()

    private lazy var sasBannerView: SASIA_Ad? = {
        let bannerView = SASIA_Ad(for: self,
                                  withFrame: CGRect(origin: .zero,
                                                    size: adViewSize))
        return bannerView
    } ()

    private lazy var sasAdRequest: SASIA_AdRequest? = {
         guard let viewModel = viewModel else { return nil }
         var tagsDictionary = viewModel.tagDictionary

         if let adIdentifier = viewModel.adIdentifier {
             tagsDictionary?["kuid"] = adIdentifier
         }

         if let baseDomain = viewModel.baseDomain {
             SASIA_AdRequest.setDomain(baseDomain)
         }

         if let customerId = viewModel.customerId {
             SASIA_AdRequest.setCustomerId(customerId)
         }

         return SASIA_AdRequest(tags: tagsDictionary)
     } ()


    // MARK: - SASAdViewController

    override func viewDidLoad() {
        super.viewDidLoad()
        guard let adModel = sasAdModel else { return }
        viewModel = SASAdViewModel(with: adModel)
        configureAdContainer()
        configureAdView()
    }

    private func configureWebView() {
        SASIA_MRAIDWebView.setMraidTracing(false)
        sasBannerView?.webView.configuration.allowsInlineMediaPlayback = true
    }

    private func configureAdView() {
        guard let request = sasAdRequest else { return }
        configureWebView()
        sasBannerView?.translatesAutoresizingMaskIntoConstraints = false
        sasBannerView?.load(request)
    }

    private func configureAdContainer() {
        guard let viewModel = viewModel else { return }
        adContainer.addContainedSubview(sasBannerView)
        adContainerHeightConstraint.constant = adViewSize.height
        adContainerWidthConstraint.constant = adViewSize.width
        adContainer.backgroundColor = UIColor.init(hex: viewModel.adBackgroundColour)
        adContainer.translatesAutoresizingMaskIntoConstraints = false
    }
}
