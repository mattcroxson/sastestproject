//
//  SASAdViewModel.swift
//  SASTestProject
//
//  Created by Matt Croxson on 26/8/19.
//  Copyright © 2019 carsales.com Ltd. All rights reserved.
//

import AdSupport
import Foundation

struct SASAdViewModel {

    private let model: SASAdModel

    var adHeight: Float? {
        guard let height = model.height else { return nil }
        return Float(height)
    }

    var adWidth: Float? {
        guard let width = model.width else { return nil }
        return Float(width)
    }

    var adIdentifier: String? {
        guard ASIdentifierManager.shared().isAdvertisingTrackingEnabled else { return nil }
        return ASIdentifierManager.shared().advertisingIdentifier.uuidString
    }

    var adBackgroundColour: String? {
        return model.backgroundColour
    }

    var baseDomain: String? {
        return model.baseDomain
    }

    var customerId: String? {
        return model.customerId
    }

    var tagDictionary: [String: String]? {
        let dictionary = model.tags?.reduce([String: String]()) { (existingDictionary, newPair) -> [String: String] in
            guard let key = newPair.key else { return existingDictionary }
            var dictionaryCopy = existingDictionary
            dictionaryCopy[key] = newPair.value
            return dictionaryCopy
        }

        return dictionary
    }

    init(with model: SASAdModel) {
        self.model = model
    }
}
