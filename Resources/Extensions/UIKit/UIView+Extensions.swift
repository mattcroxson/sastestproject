//
//  UIView+Extensions.swift
//  SASTestProject
//
//  Created by Matt Croxson on 26/8/19.
//  Copyright © 2019 carsales.com Ltd. All rights reserved.
//

import UIKit

extension UIView {
    func addContainedSubview(_ viewToAdd: UIView?) {
        guard let viewToAdd = viewToAdd else { return }
        addSubview(viewToAdd)

        let topConstraint = NSLayoutConstraint(item: self,
                                               attribute: .top,
                                               relatedBy: .equal,
                                               toItem: viewToAdd,
                                               attribute: .top,
                                               multiplier: 1,
                                               constant: 0)
        
        let bottomConstraint = NSLayoutConstraint(item: self,
                                                  attribute: .bottom,
                                                  relatedBy: .equal,
                                                  toItem: viewToAdd,
                                                  attribute: .bottom,
                                                  multiplier: 1,
                                                  constant: 0)

        let leadingConstraint = NSLayoutConstraint(item: self,
                                                   attribute: .leading,
                                                   relatedBy: .equal,
                                                   toItem: viewToAdd,
                                                   attribute: .leading,
                                                   multiplier: 1,
                                                   constant: 0)

        let trailingConstraint = NSLayoutConstraint(item: self,
                                                    attribute: .trailing,
                                                    relatedBy: .equal,
                                                    toItem: viewToAdd,
                                                    attribute: .trailing,
                                                    multiplier: 1,
                                                    constant: 0)

        NSLayoutConstraint.activate([topConstraint,
                                     bottomConstraint,
                                     leadingConstraint,
                                     trailingConstraint])
    }
}
