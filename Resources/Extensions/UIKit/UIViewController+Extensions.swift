//
//  UIViewController+Extensions.swift
//  SASTestProject
//
//  Created by Matt Croxson on 26/8/19.
//  Copyright © 2019 carsales.com Ltd. All rights reserved.
//

import UIKit

extension UIViewController {
    func simpleAlert(_ text: String?) {
        let alertController = UIAlertController(title: text, message: nil, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        present(alertController, animated: true, completion: nil)
    }
}
