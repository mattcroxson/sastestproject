//
//  SASIA_InterstitialAd.h
//  SASIA SDK
//
//  Created by M. David Blythe on 2/22/12.
//  Copyright (c) 2012 SAS Institute Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SASIA_AbstractAd.h"

@class SASIA_Interstitial;

@interface SASIA_InterstitialAd : SASIA_AbstractAd

/**
 The animation style to use when animating the appearance of this ad (via showFromController).  The default is UIModalTransitionStyleCoverVertical.
 */
@property (nonatomic, assign) UIModalTransitionStyle interstitialTransitionStyle;

- (id) init;
/**
 Presents this ad as a full-screen interstitial covering the regular application user interface.  Loading of an ad (via load) can be done before or after this method is called.
 @param hostController is the app’s UIViewController requesting showing this InterstitialAd.
 */
- (void) showFromController:(UIViewController *)hostController;

@end
