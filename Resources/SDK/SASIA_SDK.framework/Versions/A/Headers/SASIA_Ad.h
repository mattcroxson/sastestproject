//
//  SASIA_Ad.h
//  SASIA_SDK
//
//  Created by M. David Blythe on 8/24/12.
//  Copyright (c) 2012 SAS Institute Inc. All rights reserved.
//

#import "SASIA_AbstractAd.h"

@interface SASIA_Ad : SASIA_AbstractAd

/**
 Returns the view hosting presentation of the ad.  This method is provided for compatibility with previous versions, as the SASIA_Ad is itself the UIView hosting the ad.
 This view should be added to the app’s UIViewController’s view hierarchy, as necessary, to mix this ad’s display with other app content on the same screen.  Loading of an ad into this view (via load) can be done before or after the view has been added to the UIViewController’s view hierarchy.
 */
@property (atomic, readonly) UIView *view;

/**
 initForController will initialize the ad view for a specific hosting UIViewController
 @param hostViewController is the app’s UIViewController that will include this ad’s view in its view hierarchy.
 @param frame is the frame to be applied to the ad’s view.
 */
- (id) initForController:(UIViewController *)hostViewController withFrame:(CGRect)frame;

@end
