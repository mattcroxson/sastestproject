//
//  SASIA_AbstractAd.h
//  aiMatch Mobile SDK
//
//  Created by David Blythe on 2/21/12.
//  Copyright (c) 2012 SAS Institute Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SASIA_AdRequest.h"
#import "SASIA_MRAIDWebView.h"

/////////////////////////////////////////////////////////////////////////////////////////

@class SASIA_AbstractAd;
@class SASIA_Interstitial;
@class SASIA_InterstitialWebBrowser;
@class SASIA_MRAIDWebView;


/**
 To receive callbacks, an app must create an object implementing the SASIA_AdDelegate protocol, implementing whichever methods are of interest.
 */
@protocol SASIA_AdDelegate <NSObject>

@optional
/**
 Called after an ad succeeds loading an advertisement (initiated by load), and the ad loaded is not a default.
 Typically, this should trigger adding the ad view to the user interface, or animating it into position, etc.
 @param ad is the ad just loaded.
 */
- (void) didLoad:(SASIA_AbstractAd *_Nonnull)ad;
/**
 Called after an ad succeeds loading an advertisement (initiated by load), but the ad loaded is a “default” ad.  Receipt of a default ad means that the ad server currently had no other ad to serve that could fulfill the SASIA_AdRequest made.
 @param ad is the ad just loaded.
 */
- (void) didLoadDefault:(SASIA_AbstractAd *_Nonnull)ad;
/**
 Called after an ad fails loading an advertisement (initiated by load).
 @param ad is the ad that failed to load.
 @param error describes the error.
 @param failingUrl is the URL of the resource that could not be loaded.
 */
- (void) didFailLoad:(SASIA_AbstractAd *_Nonnull)ad error:(NSError *_Nonnull)error failingUrl:(NSString *_Nullable)failingUrl;
/**
 Called when an ad is about to close.  This occurs when a user touches a SASIA_InterstitialAd’s close icon, or when an MRAID-compliant ad asks to be closed, or when the ad’s close method is called.
 @param ad is the ad that is about to close.
 
 Returning YES for a SASIA_InterstitialAd allows it to remove itself from the screen. Returning NO blocks it from closing.
 A SASIA_Ad takes no action itself upon close, but this method allows the ad’s UIViewController to take some action to close the ad, if it desires. Returning YES only changes the ad’s MRAID state to “hidden”, and returning NO leaves the state alone.
 */
- (BOOL) willClose:(SASIA_AbstractAd *_Nonnull)ad;
/**
 Called when an ad is closed.
 @param ad is the ad that was closed.
 */
- (void) didClose:(SASIA_AbstractAd *_Nonnull)ad;
/**
Called when an ad is about to initiate an action in response to a user touching some portion of the ad, or in response to an MRAID-compliant ad calling the open() method.
@param ad is the ad whose action is about to begin.
@param url is the destination URL for the action.
*/
- (BOOL) willBeginAction:(SASIA_AbstractAd *_Nonnull)ad url:(NSString *_Nonnull)url;
/**
 Called when an ad action’s interstitial view is dismissed. When the ad action was shown in Safari, this method is not called.
 @param ad is the ad whose action has just finished.
 */
- (void) didFinishAction:(SASIA_AbstractAd *_Nonnull)ad;
/**
Called when an MRAID-compliant ad is about to expand itself to cover the current screen.
Returning NO blocks the expansion from occurring.
@param ad is the ad that will expand.
@param url is null if the ad will simply render itself in the expanded area. If non-null, then it is the destination URL of additional content that will be displayed in the expanded area.
*/
- (BOOL) willExpand:(SASIA_AbstractAd *_Nonnull)ad url:(NSString *_Nonnull)url;
/**
 Called when an ad’s expanded display has closed.
 @param ad is the ad whose expanded display has just closed.
 */
- (void) didFinishExpand:(SASIA_AbstractAd *_Nonnull)ad;
/**
 Called when an MRAID-compliant ad is about to resize itself and break out of its current screen.
 @param ad is the ad that will resize.
 @param size gives the position and size the ad will adopt after being resized.
 */
- (BOOL) willResize:(SASIA_AbstractAd *_Nonnull)ad size:(CGRect)size;
/**
 Called when an ad’s resized display has closed and the ad has returned to its former position and size.
 @param ad is the ad whose resized display has just closed.
 */
- (void) didFinishResize:(SASIA_AbstractAd *_Nonnull)ad;

@end

/////////////////////////////////////////////////////////////////////////////////////////

@interface SASIA_AbstractAd : UIView<SASIA_MRAIDWebViewDelegate> {
@protected
	SASIA_MRAIDWebView *_mraidView;
}

/**
 The delegate for the ad.  Defaults to nil (no delegate).
 The SASIA_AbstractAd only maintains a weak reference to this object.  The app should maintain a strong reference while the delegate is assigned to this ad.
 */
@property (nonatomic, weak) IBOutlet id<SASIA_AdDelegate> _Nullable delegate;

/**
The controller of the view for this ad.  For a SASIA_Ad, this is the same UIViewController given to its initializer.  For a SASIA_InterstitialAd, this is the controller of the interstitial view of the ad. This can be set using InterfaceBuilder to easily add the ad through drag and drop in the UI layout.
 */
@property (nonatomic, assign) IBOutlet UIViewController * _Nullable hostViewController;

/**
 The controller of the view for this ad.
 */
@property (nonatomic, readonly) UIViewController * _Nonnull viewController;
/**
 The underlying WkWebView that renders the ad.  This view is a subview of the top level ad view and should not be added to the app’s view hierarchy directly.  Access is provided here in cases where the app needs to further customize settings in the WkWebView.
 */
@property (nonatomic, readonly) WKWebView * _Nonnull webView;
/**
 The internal SASIA id of the flight creative (ad) loaded.  Note the FCID is only known for ads set to use beacon counting.
 =-2 when an ad with an unknown FCID is loaded.
 = -1 when no ad is loaded.
 = 0 when a default is loaded.
 >0 when an ad with a known FCID is loaded.
 */
@property (nonatomic, readonly) NSInteger fcid;
/** YES when a non-default ad is successfully loaded.
 */
@property (nonatomic, readonly, getter=isLoaded) BOOL loaded;
/** YES when a default ad is successfully loaded.
 */
@property (nonatomic, readonly, getter=isDefaultLoaded) BOOL defaultLoaded;
/**
 The controller associated with display of the ad’s action view.  This is non-nil only when a user has touched an ad and caused its action view to be presented within the app.
 */
@property (nonatomic, readonly) UIViewController * _Nullable actionViewController;
/**
 When NO (the default), an ad’s action will be presented within the app as a full-screen interstitial.
 When YES, the action will be presented in Safari, and the app is sent to the background.
 */
@property (nonatomic, assign, getter=isActionInBrowser) BOOL actionInBrowser;
/**
 The animation style to use when animating the appearance of this ad’s “action” view, which appears as a full-screen interstitial.  The default is UIModalTransitionStyleCoverVertical.
 */
@property (nonatomic, assign) UIModalTransitionStyle actionTransitionStyle;
/**
 YES when the ad has switched to its in-app “action” mode, wherein another view is temporarily presented on top of the regular application user interface, in response to the user touching this ad, and for the purpose of further engagement with the ad or advertiser.
 This property is not set when an ad action is launched in the device’s browser (actionInBrowser is YES).
 */
@property (nonatomic, readonly, getter=isActionInProgress) BOOL actionInProgress;

/**
 Initiates asynchronous loading and rendering of a new ad in the ad’s view.
 Upon successfully loading a non-default ad, the delegate’s didLoad: method is called.
 Upon successfully loading a default ad, the delegate’s didLoadDefault: method is called.
 Upon failure, the delegate’s didFailLoad:error:failingUrl: method is called.
 @param adRequest a SASIA_AdRequest object intialized for the requested content.
 */
- (void) load:(SASIA_AdRequest *_Nonnull)adRequest;
/**
Requests closing presentation of this ad. The SASIA_AdDelegate’s willClose and didClose methods are called and have the effect as described in the SASIA_AdDelegate API section below.
This method has the same effect as an MRAID-compliant ad calling the MRAID close() method.
For a SASIA_InterstitialAd, this method is equivalent to the user touching the ad’s close icon.
*/
- (void) close;
/**
 Cancels any in-app action initiated from this ad.  If such an action is in progress, the action’s view is removed, allowing the application’s user interface to become active again. If an ad action is launched in Safari (adActionInBrowser is YES), this method has no effect.
 */
- (void) cancelAction;

/**
 Executes arbitrary JavaScript in the underlying WkWebView holding the most recently loaded ad. This method can be used to inspect the ad’s contents, manipulate it, etc.
 The js string, if non-null, must consist of complete JavaScript statements, functions, etc.  This JavaScript is executed immediately before evaluating the jsStringExpression.
 The jsStringExpression, if non-null, must be a JavaScript expression yielding a string.  The result of evaluating this expression is returned in the completion handler.  If nil, a nil string is returned.
 
 The JavaScript is executed asynchronously. The completion handler is used to get the result of the execution.
 @param js initial javascript that is executed before the jsStringExpression
 @param jsStringExpression javascript that is executed after js, The resuts are sent to the completion handler
 @param completionHandler This is the closure that is called once all the javascript parameters have been executed.
 */
- (void) executeJavaScript:(NSString *_Nullable)js jsStringExpression:(NSString *_Nullable)jsStringExpression completionHandler:(void (^ _Nullable)(id _Nullable, NSError * _Nullable error)) completionHandler;
 /**
 Informs the ad of changes to its visibility, typically determined by the UIViewController hosting it. This method exists solely to allow informing MRAID-compliant ads of visibility changes, so that they may adjust their behavior when becoming visible or not visible.  This happens automatically for a SASIA_InterstitialAd, but a SASIA_Ad needs the assistance of its parent UIViewController.
 */
- (void) didChangeVisibility:(BOOL)newVisible;
/**
 true if this is an interstitial ad.
 */
-(BOOL) isInterstitial;

@end
