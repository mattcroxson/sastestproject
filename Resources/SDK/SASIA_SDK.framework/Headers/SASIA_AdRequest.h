//
//  SASIA_AdRequest.h
//  aiMatch Mobile SDK
//
//  Created by David Blythe on 2/21/12.
//  Copyright (c) 2012 SAS Institute Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 SASIA-AdRequest is the object used to request the ad content
 */
@interface SASIA_AdRequest : NSObject

@property (nonatomic, readonly) NSURL *baseURL;

/**
 Setter for the ad serving domain used in all subsequently-created requests.
 If the domain doesn’t start with “http://” or “https://”, “https://” is assumed.
 This method must be called once before instantiating any SASIA_AdRequest objects.
 @param domain should start with https://
 */
+ (void) setDomain:(NSString *)domain;
/**
 returns the default domain
 */
+ (NSString *) domain;

/**
 Setter for the customer’s SASIA id used in all subsequently-created requests.
 This method must be called once before instantiating any SASIA_AdRequest objects.
 @param customerId The customerID obtained from IA
 */
+ (void) setCustomerId:(NSString *)customerId;
/**
 returns the default customer id
 */
+ (NSString *) customerId;

/**
 Initializer for data encapsulating an ad request.
 tags is the complete collection of tags and values to supply in the ad request, represented as key/value pairs. The tag names and their values should be the same would be required for conventional SASIA ad serving.
 For tags without values (such as NOCOMPANION), supply an NSNull for the value (i.e. [NSNull null]).
 @param tags is a dictionary of name value pairs to be added to the request
 */
- (id) initWithTags:(NSDictionary *)tags;

/** initializes the adRequest with an already constructed URL. This does not use the default domain and customerId
 @param url is fully constructed including the domain and customerId
 @param tags is a dictionary of name value pairs to be added to the request
 */
- (id) initWithURL:(NSString *)url tags:(NSDictionary *)tags;

/** initializes the adRequest with an already constructed URL. This does not use the default domain and customerId
 This doesnot have the tags added to it
 */
- (id) initWithURL:(NSString *)urlString;

/**
 initWithDomain: customerId: tags: creates the SASIA_AdRequest fully formed
 @deprecated This method is deprecated. Use the static setDomain: and setCustomerId: methods instead.
*/
- (id) initWithDomain:(NSString *)domain customerId:(NSString *)customerId tags:(NSDictionary *)tags __deprecated_msg("Use the static setDomain: and setCustomerId: methods instead.");

@end
